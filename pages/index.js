import { graphql } from "gatsby";
import { MDXRenderer } from "gatsby-plugin-mdx";
import React from "react";

export default function Home({
  data: {
    allMdx: { edges },
  },
}) {
  return (
    <section>
      {edges.map((edge) => (
        <article>
          <MDXRenderer>{edge.node.body}</MDXRenderer>
          <hr />
        </article>
      ))}
    </section>
  );
}

export const pageQuery = graphql`
  query posts {
    allMdx {
      edges {
        node {
          body
        }
      }
    }
  }
`;
